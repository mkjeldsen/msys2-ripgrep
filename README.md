# Unmaintained

This package is no longer maintained.

# ripgrep (rg) MSYS2 package

An MSYS2 package for the official prebuilt 64-bit MSVC [`ripgrep`][ripgrep]
binary and its documentation.

From the official description:

> `ripgrep` is a command line search tool that combines the usability of The
> Silver Searcher (an `ack` clone) with the raw speed of GNU grep. `ripgrep`
> has first class support on Windows, Mac and Linux, with binary downloads
> available for [every release][releases].

The binary requires [the Visual C++ Redistributable][dll]. If `ripgrep` fails
to execute due to a missing DLL, install that and try again.

Parts of the `PKGBUILD` file are taken directly from the `PKGBUILD` distributed
with the `ripgrep` source, released under the MIT/Expat license.

[ripgrep]: https://github.com/BurntSushi/ripgrep
[releases]: https://github.com/BurntSushi/ripgrep/releases
[dll]: https://download.microsoft.com/download/9/3/F/93FCF1E7-E6A4-478B-96E7-D4B285925B00/vc_redist.x64.exe
