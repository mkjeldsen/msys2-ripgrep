# Maintainer: Mikkel Kjeldsen <commonquail@gmail.com>

pkgname=ripgrep
pkgver=0.4.0
pkgrel=1
pkgdesc='A search tool that combines the usability of The Silver Searcher with the raw speed of grep.'
arch=('x86_64')
url='http://blog.burntsushi.net/ripgrep/'
license=('MIT')
options=('!emptydirs')
source=(https://github.com/BurntSushi/$pkgname/archive/$pkgver.tar.gz
        https://github.com/BurntSushi/$pkgname/releases/download/$pkgver/$pkgname-$pkgver-$CARCH-pc-windows-msvc.zip)
sha256sums=('e93a6b59e38bc7912249175ab58ad7af0052a444b3c2c08a846fabba003414d6'
            '98626bdf07a7687564361dce2409072eb6d399763ff6db2a5bbb5384fa0dc594')
# See prepare()
noextract=("$pkgver.tar.gz")

prepare() {
  # The official package contains a symlink which causes trouble for Windows
  # during extraction. Skip automatic extraction in favour of customized manual
  # extraction -- we don't need the problematic file.
  bsdtar -xf $pkgver.tar.gz --exclude HomebrewFormula
}

package() {
  install -Dm755 rg.exe "$pkgdir/usr/bin/rg.exe"

  # From official ripgrep PKGBUILD.
  cd $pkgname-$pkgver
  install -Dm644 "doc/rg.1" "$pkgdir/usr/share/man/man1/rg.1"
  install -Dm644 "README.md" "$pkgdir/usr/share/doc/$pkgname/README.md"
  install -Dm644 "COPYING" "$pkgdir/usr/share/doc/$pkgname/COPYING"
  install -Dm644 "LICENSE-MIT" "$pkgdir/usr/share/doc/$pkgname/LICENSE-MIT"
  install -Dm644 "UNLICENSE" "$pkgdir/usr/share/doc/$pkgname/UNLICENSE"
  install -Dm644 "CHANGELOG.md" "$pkgdir/usr/share/doc/$pkgname/CHANGELOG.md"
}
